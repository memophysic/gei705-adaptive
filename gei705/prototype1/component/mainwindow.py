from functools import partial

from PyQt5.QtWidgets import QMainWindow, QPushButton, QPlainTextEdit

from prototype1.definitions.markov_chain import MarkovChain
from prototype1.definitions.view import qt_type_from_ui


class MainWindow(QMainWindow, qt_type_from_ui("mainwindow.ui")):

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.setWindowTitle("Markov chain demonstration")

        self.plainTextEdit = self.plainTextEdit
        """:type: QPlainTextEdit"""

        self.actionButtons = [
            self.action1,
            self.action2,
            self.action3,
            self.action4,
            self.action5
        ]
        """:type: List[QPushButton]"""

        for b in self.actionButtons:
            b.clicked.connect(partial(self.actionPressed, b))

        self._markov_chain = MarkovChain(self.actionButtons)
        self._current_prediction = None
        """:type: QPushButton"""

    def actionPressed(self, actionButton):
        # Create interaction log entry
        self.plainTextEdit.insertPlainText(
            "Button pressed: {}\n".format(actionButton.text())
        )
        self._markov_chain.add_interaction(actionButton)
        self.updateNextPrediction()

    def updateNextPrediction(self):
        for ab in self.actionButtons:
            ab.setStyleSheet("")

        prediction = self._markov_chain.get_prediction()
        """:type: QPushButton"""
        if prediction:
            print("Prediction: {}".format(prediction.text()))
            prediction.setStyleSheet("background-color: rgba(0, 200, 100, "
                                     "33%);")
            for ab in self.actionButtons:
                if ab is not prediction:
                    probability = self._markov_chain.get_probability(ab) * 100
                    print(probability)
                    css_prob = min(100 + probability * 3, 255)
                    ab.setStyleSheet(
                        "background-color: rgba({}, {}, {});".format(
                            css_prob, css_prob * 0.5, css_prob * 0.2))

            self._current_prediction = prediction

