from prototype1.definitions.view import ViewQt
ViewQt.initialize_application()

from prototype1.component.mainwindow import MainWindow


view = ViewQt(MainWindow)
view.launch()
