from prototype1.definitions.markov_chain import MarkovChain


class Target:
    def __init__(self, id):
        self._id = id

    def __str__(self):
        return "Target [{}]".format(self._id)


targets = [Target(i) for i in range(5)]

mc = MarkovChain(targets)
print(mc.get_prediction())

mc.add_interaction(targets[4])
print(mc.get_prediction())