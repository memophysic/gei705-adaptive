import operator


class MarkovChain:

    DAMP_FACTOR = 0.8

    def __init__(self, targets):
        self._last_target = None

        self._chain = {t: {} for t in targets}
        for t in self._chain:
            self._chain[t].update({t: 0.0 for t in targets})

    def add_interaction(self, target):
        if self._last_target:
            for k in self._chain.keys():
                self._chain[self._last_target][k] *= self.DAMP_FACTOR
            self._chain[self._last_target][target] += 1.0

        self._last_target = target

    def get_prediction(self, target=None):
        prediction = None

        if self._last_target:
            if not target:
                target = self._last_target
            predictions = max(self._chain[target].items(),
                              key=operator.itemgetter(1))
            if predictions[1] > 0.0:
                prediction = predictions[0]
        return prediction

    def get_probability(self, next_target, target=None):
        if not target:
            target = self._last_target

        s = sum(self._chain[target][k] for k in self._chain.keys())
        return self._chain[target][next_target] / s
