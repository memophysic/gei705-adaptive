from collections import OrderedDict


class AddressBook:
    def __init__(self):
        self._entries = OrderedDict()

    def add_entry(self, name, phone, address="", email=""):
        self._entries[name] = {"phone": phone,
                               "address": address,
                               "email": email}

    def remove_entry(self, name):
        self._entries.pop(name)

    def update_name(self, name, new_name):
        if name != new_name:
            self._entries[new_name] = self._entries[name]
            self.remove_entry(name)

    def update_phone(self, name, phone):
        self._entries[name]["phone"] = phone

    def update_address(self, name, address):
        self._entries[name]["address"] = address

    def update_email(self, name, email):
        self._entries[name]["email"] = email

    def get_entry(self, name):
        return self._entries[name].copy()

    def get_entries(self):
        return self._entries.copy()


class LoggedAddressBook(AddressBook):
    def __init__(self):
        super().__init__()
        # Key: Name, Value: Modified fields
        self._modified_entries = {}

    def _add_mod_field(self, name, field):
        mod_fields = self._modified_entries.get(name)
        if mod_fields:
            self._modified_entries[name] = mod_fields.union({field})
        else:
            self._modified_entries[name] = {field}

    def update_name(self, name, new_name):
        super().update_name(name, new_name)
        if name != new_name:
            mod_fields = self._modified_entries.get(name)
            if mod_fields:
                self._modified_entries[new_name] = mod_fields.union({"name"})
            else:
                self._modified_entries[new_name] = {"name"}
            self._modified_entries.pop(name, None)

    def update_phone(self, name, phone):
        if phone != self.get_entry(name).get("phone"):
            self._add_mod_field(name, "phone")
        super().update_phone(name, phone)

    def update_address(self, name, address):
        if address != self.get_entry(name).get("address"):
            self._add_mod_field(name, "address")
        super().update_address(name, address)

    def update_email(self, name, email):
        if email != self.get_entry(name).get("email"):
            self._add_mod_field(name, "email")
        super().update_email(name, email)

    def reset(self):
        self._modified_entries.clear()

    def count_modified_entries(self):
        return len(self._modified_entries)

    def count_modified_fields_per_entry(self):
        if len(self._modified_entries) > 0:
            return max(len(v) for v in self._modified_entries.values())
        else:
            return 0
