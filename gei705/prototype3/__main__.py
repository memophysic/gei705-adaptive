from prototype1.definitions.view import ViewQt
from prototype3.component.application_window import ApplicationWindow

ViewQt.initialize_application()

view = ViewQt(ApplicationWindow)
view.launch()
