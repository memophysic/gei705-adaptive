from adaptivepy.monitor.primitives import Monitor

from prototype1.definitions.markov_chain import MarkovChain
from prototype3.adaptation_model.parameters import SingularityParameter


class ControlledSingularityMonitor(Monitor):
    """
    WARNING: This monitor is stateful
    """
    def __init__(self, value):
        super().__init__()
        self._value = None
        self.set_value(value)

    @classmethod
    def possible_values(cls):
        return SingularityParameter.possible_values()

    def set_value(self, value):
        assert value in ControlledSingularityMonitor.possible_values()
        self._value = value

    def value(self):
        return self._value


class AdaptiveSingularityMonitor(Monitor):
    """
    WARNING: This monitor is stateful
    """
    def __init__(self, default_value, title=None):
        assert default_value in AdaptiveSingularityMonitor.possible_values()
        self._default_value = default_value
        self._markov_chain = MarkovChain(
            AdaptiveSingularityMonitor.possible_values())
        self._title = title or "AdaptiveSingularityMonitor"

    @classmethod
    def possible_values(cls):
        return SingularityParameter.possible_values()

    def value(self):
        return self._markov_chain.get_prediction() or self._default_value

    def add_interaction(self, target):
        print("{}: +Interaction '{}'".format(self._title, target))
        self._markov_chain.add_interaction(target)
