from enum import Enum

from adaptivepy.monitor.parameter import Parameter, DiscreteParameter


class SingularityEnum(Enum):
    single = 0
    multi = 1


class SingularityParameter(DiscreteParameter):
    @classmethod
    def possible_values(cls):
        return set(SingularityEnum)


class EntriesParameter(SingularityParameter):
    pass


class FieldsParameter(SingularityParameter):
    pass
