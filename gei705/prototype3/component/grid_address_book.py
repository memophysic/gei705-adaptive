from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QTableWidget, QTableWidgetItem
from adaptivepy.component.adaptive import Adaptive
from adaptivepy.component.definitions import AdaptationSpace

from prototype3.adaptation_model.parameters import EntriesParameter, \
    FieldsParameter, SingularityEnum
from prototype3.definitions.address_book_editor import AddressBookEditor
from prototype3.view import qt_type_from_ui
from prototype3.data_model.address_book import AddressBook


@AdaptationSpace({EntriesParameter: {SingularityEnum.multi},
                  FieldsParameter: {SingularityEnum.multi}})
class GridAddressBook(QWidget, qt_type_from_ui(
        "grid_address_book.ui"), Adaptive, AddressBookEditor):

    def __init__(self, address_book,
                 parameter_value_provider=None,
                 parent=None):
        """
        :type address_book: AddressBook
        :type parent: QWidget
        """
        QWidget.__init__(self, parent)
        Adaptive.__init__(self, parameter_value_provider)
        self.setupUi(self)
        self._model = address_book
        self._current_name = ""
        self.tableWidget = self.tableWidget
        """:type: QTableWidget"""
        self.tableWidget.itemSelectionChanged.connect(self.onSelectionChanged)

        self._columns = ["phone", "address", "email"]

        self.column_save_handlers = {
            0: lambda name, data: self._model.update_name(self._current_name,
                                                          name),
            1: lambda name, data: self._model.update_phone(name, data),
            2: lambda name, data: self._model.update_address(name, data),
            3: lambda name, data: self._model.update_email(name, data),
        }

        self.update_content()
        self.enable_save(True)

    def enable_save(self, enable):
        try:
            if enable:
                self.tableWidget.itemChanged.connect(self.onChanged,
                                                     Qt.UniqueConnection)
            else:
                self.tableWidget.itemChanged.disconnect()
        except TypeError:
            # Not connected or multiple connections, nothing needs to be done
            pass

    def update_content(self):
        self.enable_save(False)
        self.tableWidget.clearContents()
        entries = self._model.get_entries()
        row = 0
        self.tableWidget.setRowCount(len(entries))
        for name, fields in entries.items():
            self._set_entry(row, name, fields)
            row += 1
        self.enable_save(True)

    def __get_or_create_item(self, row, col):
        item = self.tableWidget.item(row, col)
        if not item:
            item = QTableWidgetItem()
            self.tableWidget.setItem(row, col, item)
        return item

    def _set_entry(self, row, name, fields):
        name_field = self.__get_or_create_item(row, 0)
        name_field.setText(name)

        col = 1
        for key in self._columns:
            self.__get_or_create_item(row, col).setText(fields.get(key))
            col += 1

    def _get_pos(self, item):
        return self.tableWidget.row(item), self.tableWidget.column(item)

    def _get_name(self, row):
        return self.tableWidget.item(row, 0).text()

    def onChanged(self, item):
        row, col = self._get_pos(item)
        self.column_save_handlers.get(col)(self._get_name(row), item.text())

    def onSelectionChanged(self):
        row = self.tableWidget.currentRow()
        self._current_name = self.tableWidget.item(row, 0).text()
