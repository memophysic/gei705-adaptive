from PyQt5.QtWidgets import QLayout

from adaptivepy.choice_strategies.restriction_based import \
    choose_most_restricted
from adaptivepy.component.adaptation_space import filter_by_adaptation_space
from adaptivepy.component.adaptive_proxyrouter import \
    AdaptiveInternalProxyRouter

from prototype3.component.grid_address_book import GridAddressBook
from prototype3.component.multi_fields_address_book import \
    MultiFieldsAddressBookWidget
from prototype3.definitions.address_book_editor import AddressBookEditor


class AdaptiveAddressBookEditor(AdaptiveInternalProxyRouter, AddressBookEditor):

    def __init__(self, address_book, container):
        """
        :type container: QLayout
        """
        super().__init__()
        self._address_book = address_book
        self._container = container
        self._subscribe_to_all_parameters()
        self.route(self.choose_route())

    def arguments_provider(self):
        arguments_provider = \
            AdaptiveInternalProxyRouter.arguments_provider(self)
        arguments_provider["address_book"] = self._address_book
        return arguments_provider

    @classmethod
    def candidates(cls, arguments_provider=None):
        arguments_provider = arguments_provider or {}
        param_val_prv = arguments_provider.get("parameter_value_provider", None)
        address_book_model = arguments_provider.get("address_book")

        return {
            MultiFieldsAddressBookWidget:
                lambda: MultiFieldsAddressBookWidget(address_book_model,
                                                     param_val_prv),
            GridAddressBook:
                lambda: GridAddressBook(address_book_model,
                                        param_val_prv)
        }

    def __del__(self):
        self._unsubscribe_from_all_parameters()

    def choose_route(self):
        snapshot = self._local_snapshot()
        valid_options = filter_by_adaptation_space(self.candidates(),
                                                   snapshot)

        # Three cases:
        if not valid_options:
            #  1) No option, return default
            route_candidate = MultiFieldsAddressBookWidget
        elif len(valid_options) == 1:
            #  2) Exactly one option, take it
            route_candidate = next(iter(valid_options))
        else:
            #  3) More than one option, choose preferable
            #     We choose the one which has the most restricted adaptation
            #     space as we assume it is most likely to be specific to
            #     the current situation
            route_candidate = choose_most_restricted(valid_options,
                                                     snapshot.keys())

        return route_candidate

    def route(self, target):
        source = self.proxy().delegate()
        if isinstance(source, target):
            target_instance = source
        else:
            target_instance = AdaptiveInternalProxyRouter.route(self, target)
            """:type: AddressBookEditor"""
            target_instance.update_content()
            if source:
                self._container.removeWidget(source)
                source.setParent(None)
            self._container.addWidget(target_instance)

        return target_instance
