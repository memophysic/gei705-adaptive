from PyQt5.QtWidgets import QMainWindow, QPushButton

from prototype3.component.edit_data_dialog import EditDataAdaptiveDialog
from prototype3.data_model.address_book import LoggedAddressBook


class ApplicationWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setWindowTitle("Adaptive component with Markov Chain demo")

        self.setFixedSize(800, 600)

        self.address_book = LoggedAddressBook()
        self.address_book.add_entry("Roger Boncoeur",
                                    "555-7894",
                                    email="roger.boncoeur@usherbrooke.ca")
        self.address_book.add_entry("Michael Forte",
                                    "555-1545",
                                    address="123 rue de la clairière")
        self.address_book.add_entry("Alex Vendimoto",
                                    "555-7416",
                                    "124 rue de la clairière",
                                    "alex.vendimoto@usherbrooke.ca")

        self.editDataDialog = EditDataAdaptiveDialog(self.address_book, self)

        self.editDataButton = QPushButton("Edit data")
        self.editDataButton.pressed.connect(self.launchEditDataDialog)
        self.setCentralWidget(self.editDataButton)

    def launchEditDataDialog(self):
        self.editDataDialog.exec()
