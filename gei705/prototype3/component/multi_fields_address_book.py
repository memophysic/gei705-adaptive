from PyQt5.QtWidgets import QWidget, QComboBox, QLineEdit
from adaptivepy.component.adaptive import Adaptive
from adaptivepy.component.definitions import AdaptationSpace

from prototype3.adaptation_model.parameters import EntriesParameter, \
    SingularityEnum, FieldsParameter
from prototype3.definitions.address_book_editor import AddressBookEditor
from prototype3.view import qt_type_from_ui
from prototype3.data_model.address_book import AddressBook


@AdaptationSpace({EntriesParameter: {SingularityEnum.single},
                  FieldsParameter: {SingularityEnum.multi}})
class MultiFieldsAddressBookWidget(QWidget, qt_type_from_ui(
        "multi_fields_address_book.ui"), Adaptive, AddressBookEditor):

    def __init__(self, address_book,
                 parameter_value_provider=None,
                 parent=None):
        """
        :type address_book: AddressBook
        :type parent: QWidget
        """
        QWidget.__init__(self, parent)
        Adaptive.__init__(self, parameter_value_provider)
        self.setupUi(self)
        self._model = address_book

        self.fields = {"name": self.name,
                       "phone": self.phone,
                       "address": self.address,
                       "email": self.email}
        """:type: dict[str, QLineEdit]"""

        self.name.editingFinished.connect(
            lambda: self.save_name(self.entries.currentText(),
                                   self.name.text()))
        self.phone.editingFinished.connect(
            lambda: self._model.update_phone(self.entries.currentText(),
                                             self.phone.text()))
        self.address.editingFinished.connect(
            lambda: self._model.update_address(self.entries.currentText(),
                                               self.address.text()))
        self.email.editingFinished.connect(
            lambda: self._model.update_email(self.entries.currentText(),
                                             self.email.text()))

        self.entries = self.entries
        """:type: QComboBox"""
        self.entries.currentIndexChanged.connect(self._update_fields)

        self.update_content()

    def update_content(self):
        self.entries.clear()
        self.entries.addItems(list(self._model.get_entries().keys()))
        self._update_fields()

    def _update_fields(self):
        current_name = self.entries.currentText()
        entry_data = {}
        if current_name:
            entry_data = self._model.get_entry(current_name)
            entry_data["name"] = current_name

        for key, field in self.fields.items():
            self.__update_field(key, entry_data.get(key, ""))

    def __update_field(self, key, new_text):
        lineedit = self.fields[key]
        lineedit.setText(new_text)

    def save_name(self, original_name, new_name):
        self._model.update_name(original_name, new_name)
        self.entries.clear()
        self.entries.addItems(list(self._model.get_entries().keys()))
        self.entries.setCurrentText(new_name)
