from PyQt5.QtWidgets import QDialog, QHBoxLayout, QPushButton
from adaptivepy.monitor.monitor_event_manager import register_global_monitor, \
    GLOBAL_MONITORS_EVENT_MANAGER
from adaptivepy.monitor.pull_dynamic_monitor import PullDynamicMonitorDecorator

from prototype3.adaptation_model.monitor import AdaptiveSingularityMonitor, \
    ControlledSingularityMonitor
from prototype3.adaptation_model.parameters import EntriesParameter, \
    FieldsParameter, SingularityParameter, SingularityEnum
from prototype3.component.adaptive_address_book_editor import \
    AdaptiveAddressBookEditor
from prototype3.component.multi_fields_address_book import \
    MultiFieldsAddressBookWidget
from prototype3.view import qt_type_from_ui
from prototype3.data_model.address_book import LoggedAddressBook


class EditDataAdaptiveDialog(QDialog,
                             qt_type_from_ui("edit_data_adaptive_dialog.ui")):
    def __init__(self, address_book, parent=None):
        """
        :type address_book: LoggedAddressBook
        :type parent: QWidget
        """
        super().__init__(parent)
        self.setupUi(self)

        self.view1Button = self.view1Button
        """:type: QPushButton"""
        self.view1Button.pressed.connect(lambda: self.setViewManual(1))

        self.view2Button = self.view2Button
        """:type: QPushButton"""
        self.view2Button.pressed.connect(lambda: self.setViewManual(2))

        self.editor_container = self.editor_container
        """:type: QHBoxLayout"""

        default = SingularityEnum.single

        self._adaptive_monitors = (AdaptiveSingularityMonitor(default,
                                                              "Entries"),
                                   AdaptiveSingularityMonitor(default,
                                                              "Fields"))
        self._controlled_monitors = (ControlledSingularityMonitor(default),
                                     ControlledSingularityMonitor(default))
        self._monitors = {
            True: (PullDynamicMonitorDecorator(self._adaptive_monitors[0]),
                   PullDynamicMonitorDecorator(self._adaptive_monitors[1])),
            False: (PullDynamicMonitorDecorator(self._controlled_monitors[0]),
                    PullDynamicMonitorDecorator(self._controlled_monitors[1])),
        }

        self.autoButton = self.autoButton
        """:type: QPushButton"""
        self.autoButton.toggled.connect(self._toggleAuto)

        self._logged_address_book = address_book
        self._editor = AdaptiveAddressBookEditor(address_book,
                                                 self.editor_container)
        self.autoButton.toggle()

    def exec(self):
        self.updateMonitors()
        super().exec()

    def _toggleAuto(self, checked):
        selected_monitors = self._monitors.get(checked)

        for param, monitor in zip((EntriesParameter, FieldsParameter),
                                  selected_monitors):
            try:
                GLOBAL_MONITORS_EVENT_MANAGER.replace_monitor(param, monitor)
            except KeyError:
                GLOBAL_MONITORS_EVENT_MANAGER.register_monitor(param, monitor)
        self.updateMonitors()

    def setViewManual(self, index):
        self.autoButton.setChecked(False)
        self._toggleAuto(False)

        # [0]=Entries, [1]=Fields
        values = (SingularityEnum.multi, SingularityEnum.multi)  # Default
        if index == 1:
            values = (SingularityEnum.single, SingularityEnum.multi)

        for i in (0, 1):
            self._controlled_monitors[i].set_value(values[i])

        self.updateMonitors()

    def updateMonitors(self):
        for dynamic_monitor in self._monitors.get(self.autoButton.isChecked()):
            dynamic_monitor.update()

    def _add_adaptive_monitor_interaction(self, counts, monitors):
        for c, m in zip(counts, monitors):
            if c:
                m.add_interaction(SingularityEnum.single if c == 1 else
                                  SingularityEnum.multi)

    def done(self, r):
        self._add_adaptive_monitor_interaction((
            self._logged_address_book.count_modified_entries(),
            self._logged_address_book.count_modified_fields_per_entry()),
            self._adaptive_monitors
        )
        self._logged_address_book.reset()
        super().done(r)
