
class InvalidParent(Exception):
    pass


class Component:
    """
    A component represents the smallest unit within a hierarchy of software
    entities which can be connected together. A simple component is a leaf,
    meaning it cannot have any children and 1 or 0 parent.
    """

    def parent(self):
        """
        :rtype: CompositeComponent | None
        """
        raise NotImplementedError()

    def set_parent(self, component):
        """
        :param component: CompositeComponent | None
        :raise: InvalidParent
        """
        raise NotImplementedError()


class CompositeComponent(Component):
    """
    Component which can contain other components as children
    It may have any number of children (including zero), but only a single
    parent. A component with no parent is at the top (root) of the hierarchy.
    """

    def add_child(self, child):
        """
        :type child: Component
        """
        raise NotImplementedError()

    def remove_child(self, child):
        """
        :type child: Component
        """
        raise NotImplementedError()

    def children(self):
        """
        :rtype: collections.Iterable[Component]
        """
        raise NotImplementedError()

    def is_ancestor_of(self, component):
        """
        :param component: CompositeComponent
        :return: True if the composite component is an ancestor (a parent,
                 grand-parent, grand-grand-parent, etc.) of the current
                 instance, False otherwise. Note that a simple Component cannot
                 be an ancestor since it cannot hold children.
        """
        lookup_component = component
        while lookup_component is not None:
            if lookup_component is self:
                return True
            lookup_component = lookup_component.parent()
        return False


class ComponentsContainer:
    """
    Generic container for components
    """

    def add(self, component):
        """
        :type component: Component
        """
        raise NotImplementedError()

    def remove(self, component):
        """
        :type component: Component
        :raise: ValueError
        """
        raise NotImplementedError()

    def get(self):
        """
        :rtype: collections.Iterable[Component]
        """
        raise NotImplementedError()

