from prototype2.definitions import Component, CompositeComponent, InvalidParent


class BaseComponent(Component):
    def __init__(self, parent=None):
        """
        :type parent: CompositeComponent | None
        """
        super().__init__()
        self._parent = None
        if parent is not None:
            self.set_parent(parent)

    def parent(self):
        return self._parent

    def set_parent(self, component):
        if self._parent is component:
            return

        if self._parent is not None:
            self._parent.remove_child(self)
        self._parent = component
        if self._parent is not None:
            self._parent.add_child(self)


class BaseCompositeComponent(BaseComponent, CompositeComponent):
    def __init__(self, container, parent=None):
        """
        :type container: ComponentsContainer
        :type parent: Component | None
        """
        super().__init__(parent)
        self._container = container

    def add_child(self, child):
        """
        :type child: Component | CompositeComponent
        """
        if child is self:
            raise InvalidParent("A component cannot be its own child")
        if isinstance(child, CompositeComponent) and child.is_ancestor_of(self):
            raise InvalidParent("A component cannot have an ancestor as child")

        self._container.add(child)
        child_parent = child.parent()
        if child_parent is not None and child_parent is not self:
            child_parent.remove_child(child)
        child._parent = self

    def remove_child(self, child):
        self._container.remove(child)
        child._parent = None

    def children(self):
        return self._container.get()
