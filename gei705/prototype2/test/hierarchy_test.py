import unittest

from prototype2.base_component import BaseComponent, BaseCompositeComponent
from prototype2.components_container import OrderedComponentsContainer
from prototype2.definitions import InvalidParent


class CompositeComponentTests(unittest.TestCase):

    def setUp(self):
        self.root = BaseCompositeComponent(OrderedComponentsContainer())

    def test_raise_on_add_itself(self):
        self.assertRaises(InvalidParent, self.root.add_child, self.root)

    def test_raise_on_add_parent(self):
        child = BaseCompositeComponent(OrderedComponentsContainer(), self.root)
        self.assertRaises(InvalidParent, child.add_child, self.root)

    def test_add_normal_child(self):
        child = BaseComponent()
        self.root.add_child(child)
        self.assertIn(child, self.root.children())
        self.assertEquals(1, len(self.root.children()))

    def test_add_composite_child(self):
        child = BaseCompositeComponent(OrderedComponentsContainer())
        self.root.add_child(child)
        self.assertIn(child, self.root.children())
        self.assertEquals(1, len(self.root.children()))

    def test_raise_on_remove_unowned_child(self):
        self.assertRaises(ValueError,
                          self.root.remove_child,
                          BaseComponent())

    def test_remove_child(self):
        child = BaseComponent()
        self.root.add_child(child)
        self.root.remove_child(child)
        self.assertNotIn(child, self.root.children())
        self.assertEquals(0, len(self.root.children()))