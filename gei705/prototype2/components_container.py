from prototype2.definitions import ComponentsContainer


class OrderedComponentsContainer(ComponentsContainer):
    """
    Components container which guarantees order of insertion is kept
    Uses a list as backend
    """

    def __init__(self):
        self._components = []

    def add(self, component):
        self._components.append(component)

    def remove(self, component):
        self._components.remove(component)

    def get(self):
        return tuple(self._components)


class UniqueComponentsContainer(ComponentsContainer):
    """
    Components container which guarantees that all its components are unique
    Uses a set as backend
    """

    def __init__(self):
        self._components = set()

    def add(self, component):
        self._components.add(component)

    def remove(self, component):
        self._components.remove(component)

    def get(self):
        return tuple(self._components)
